import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CompressorComponent} from "./components/compressor/compressor.component";

const routes: Routes = [
  { path: '', redirectTo: 'compressor', pathMatch: 'full' },
  { path: 'compressor', component: CompressorComponent },
  { path: '**', redirectTo: 'compressor', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
