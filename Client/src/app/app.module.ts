import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CompressorComponent } from './components/compressor/compressor.component';
import { HeaderComponent } from './components/header/header.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ProgressBarComponent } from './common/components/progress-bar/progress-bar.component';
import { CompressComponent } from './components/compressor/compress/compress.component';
import { DecompressComponent } from './components/compressor/decompress/decompress.component';
import { ModalComponent } from './common/components/modal/modal.component';
import {Interceptor} from "./common/interceptor/interceptor";

@NgModule({
  declarations: [
    AppComponent,
    CompressorComponent,
    HeaderComponent,
    ProgressBarComponent,
    CompressComponent,
    DecompressComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    TabsModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor, multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalComponent
  ]
})
export class AppModule { }
