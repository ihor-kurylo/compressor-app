import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {ModalService} from "../../services/modal.service";

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(
    private modalService: ModalService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error && error.error && error.error.errors) {
          this.modalService.alert( error.error.errors.join('<br/>'), 'Error');
        }
        return throwError(error);
      }));
  }
}
