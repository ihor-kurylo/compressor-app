import { Component, OnInit } from '@angular/core';
import {CompressForm} from "../../../models/compress-form";
import {CommonService} from "../../../services/common.service";
import {NgForm} from "@angular/forms";
import {ApiService} from "../../../services/api.service";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-compress',
  templateUrl: './compress.component.html',
  styleUrls: ['./compress.component.scss']
})
export class CompressComponent implements OnInit {

  public loading: boolean = false;
  public compressForm: CompressForm = new CompressForm();

  constructor(
    private apiService: ApiService,
    private commonService: CommonService
  ) { }

  ngOnInit(): void { }

  compressString(form: NgForm): void {
    if (form.valid) {
      this.loading = true;
      this.commonService.toggleLoading(true);
      this.compressForm.compressedString = '';

      this.apiService.compress(form.value.inputString)
        .pipe(
          finalize(() => {
            this.loading = false;
            this.commonService.toggleLoading(false);
          })
        )
        .subscribe(res => {
          this.compressForm.compressedString = res.result;

          form.reset();
        });
    }
  }

  clear(form: NgForm): void {
    form.reset();
    this.compressForm = new CompressForm();
  }

}
