import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecompressComponent } from './decompress.component';

describe('DecompressComponent', () => {
  let component: DecompressComponent;
  let fixture: ComponentFixture<DecompressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecompressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecompressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
