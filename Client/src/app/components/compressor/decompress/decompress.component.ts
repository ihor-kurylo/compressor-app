import { Component, OnInit } from '@angular/core';
import {DecompressForm} from "../../../models/decompress-form";
import {CommonService} from "../../../services/common.service";
import {NgForm} from "@angular/forms";
import {ApiService} from "../../../services/api.service";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-decompress',
  templateUrl: './decompress.component.html',
  styleUrls: ['./decompress.component.scss']
})
export class DecompressComponent implements OnInit {

  public loading: boolean = false;
  public decompressForm: DecompressForm = new DecompressForm();

  constructor(
    private apiService: ApiService,
    private commonService: CommonService
  ) { }

  ngOnInit(): void { }

  decompressString(form: NgForm): void {
    if (form.valid) {
      this.loading = true;
      this.commonService.toggleLoading(true);


      this.apiService.decompress(form.value.inputString)
        .pipe(
          finalize(() => {
            this.loading = false;
            this.commonService.toggleLoading(false);
          })
        )
          .subscribe(res => {
          this.decompressForm.decompressedString = res.result;

          form.reset();
        });
    }
  }

  clear(form: NgForm): void {
    form.reset();
    this.decompressForm = new DecompressForm();
  }

}
