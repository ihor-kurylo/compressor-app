import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {CompressResponse} from "../models/compress-response";
import {DecompressResponse} from "../models/decompress-response";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private baseApiURL: string = environment.baseApiURL;

  constructor(
    private http: HttpClient
  ) { }

  compress(string: string): Observable<CompressResponse> {
    return this.http.post<CompressResponse>(this.baseApiURL + '/compressor/compress', { string });
  }

  decompress(string: string): Observable<DecompressResponse> {
    return this.http.post<DecompressResponse>(this.baseApiURL + '/compressor/decompress', { string });
  }

}
