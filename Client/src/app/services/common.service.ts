import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

  isLoading(): Observable<boolean> {
    return this.loading$.asObservable();
  }

  toggleLoading(loading: boolean): void {
    this.loading$.next(loading);
  }

}
