import { Injectable } from '@angular/core';
import {BsModalService} from "ngx-bootstrap/modal";
import {ModalComponent} from "../common/components/modal/modal.component";

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(
    private modalService: BsModalService
  ) { }

  private showModal(message: string, title?: string) {
    this.modalService.show(ModalComponent, {initialState: {
        title: title || 'Message',
        message
      }});
  }

  public alert(message: string, title?: string) {
    return this.showModal(message, title);
  }

}
