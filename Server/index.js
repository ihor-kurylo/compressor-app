const server = require('./server/server');

const PORT = process.env.PORT || 3000;

function start() {
    try {
        server.listen(PORT, () => {
            console.log('Server is running on port ' + PORT);
        });
    } catch (e) {
        console.error(e);
    }
}

start();
