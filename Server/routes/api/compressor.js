const {Router} = require('express');
const {validationResult} = require('express-validator');
const {compressValidators, decompressValidators} = require('../../utils/validators');

const router = Router();

router.post('/compress', compressValidators, async (req, res) => {
    try {
        const {string} = req.body;

        try {
            await validatorsErrorHandler(req, res);
        } catch (e) {
            return false;
        }

        res.status(200).json({ result: getCompressedString(string) });
    } catch (e) {
        console.log(e);
        res.status(500).json(e);
    }
});

router.post('/decompress', decompressValidators, async (req, res) => {
    try {
        const {string} = req.body;

        try {
            await validatorsErrorHandler(req, res);
        } catch (e) {
            return false;
        }

        res.status(200).json({ result: getDecompressedString(string) });
    } catch (e) {
        console.log(e);
        res.status(500).json(e);
    }
});

function validatorsErrorHandler(req, res) {
    return new Promise((resolve, reject) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const errorMsg = [];
            errors.array().forEach(error => errorMsg.push(error.msg));
            reject(true);
            return res.status(422).json({
                errors: errorMsg
            });
        }
        resolve(true);
    });
}

function getCompressedString(string) {
    const symbolArray = string.trim().split('');
    const substringArray = [];
    let prevSymbol;
    let substring = '';

    symbolArray.forEach(symbol => {
        const _symbol = symbol.toLowerCase();
        if (prevSymbol && prevSymbol !== _symbol) {
            substringArray.push(substring);
            substring = '';
        }

        substring += _symbol;
        prevSymbol = _symbol;
    });
    substringArray.push(substring);

    let result = '';
    substringArray.forEach(substr => {
        result += substr.length > 2 ? (substr[0] + substr.length) : substr;
    });

    return result;
}

function getDecompressedString(string) {
    return string.trim().toLowerCase().replace(/[0-9]+/g, (value, index) => {
        let result = '';
        for (let i = 1; i < +value; i++) {
            result += string[index - 1];
        }
        return result;
    });
}

module.exports = router;
