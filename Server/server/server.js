const express = require('express');
const compression = require('compression');
const path = require('path');
const bodyParser = require("body-parser");

const compressorRoutes = require('../routes/api/compressor');

const notFoundHandler = require('../middleware/not-found');
const allowCrossDomain = require('../middleware/allow-cross-domain');

const server = express();

server.use(compression());
server.use(allowCrossDomain);

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

server.use(express.static(path.join(__dirname, 'public')));

server.use('/api/compressor', compressorRoutes);

server.use(notFoundHandler);

module.exports = server;
