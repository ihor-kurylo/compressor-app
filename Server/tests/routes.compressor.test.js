const request = require('supertest');
const app = require('../server/server');

describe('Compress endpoint', () => {
    it('should return \'a4b4c4\'', async () => {
        const res = await request(app)
            .post('/api/compressor/compress')
            .send({
                string: 'aaaabbbbcccc',
            })
            expect(res.body.result).toEqual('a4b4c4');
    });
});

describe('Decompress endpoint', () => {
    it('should return \'aabbbcccc\'', async () => {
        const res = await request(app)
            .post('/api/compressor/decompress')
            .send({
                string: 'aab3c4',
            })
            expect(res.body.result).toEqual('aabbbcccc');
    });
});
