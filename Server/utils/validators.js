const {body} = require('express-validator');

exports.compressValidators = [
    body('string')
        .exists()
        .withMessage('Invalid field name')
        .isString()
        .withMessage('Invalid data type')
        .matches(/^[a-fA-F]+$/)
        .withMessage('Field should includes next chars (a-f)')
];

exports.decompressValidators = [
    body('string')
        .exists()
        .withMessage('Invalid field name')
        .isString()
        .withMessage('Invalid data type')
        .matches(/[^0-9]([a-fA-F0-9]+)?$/)
        .withMessage('Field cannot begin with a number. Field should includes numbers and next chars (a-f)')
];
